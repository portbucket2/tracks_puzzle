using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Junction : MonoBehaviour
{
    public float speed;

    public GameObject root;
    public Track[] tracks;
    public float[] rotations;
    public int current_path;

    private float target_rotation = 0;
    private float current_rotation = 0;
    private float interpolation = 0;

    private GameManager gameManager;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();

        target_rotation = rotations[current_path];
        current_rotation = rotations[current_path];
        root.transform.localRotation = Quaternion.Euler(Vector3.up * current_rotation);
    }

    private void OnMouseDown()
    {
        if (gameManager.failed || gameManager.levelCompleted)
        {
            return;
        }

        if (gameManager.train_running)
        {
            return;
        }

        gameManager.TutorialNext();

        current_path++;
        if (current_path >= rotations.Length)
        {
            current_path = 0;
        }

        target_rotation = rotations[current_path];
        interpolation = 0;
    }

    private void FixedUpdate()
    {
        if (interpolation < 1)
        {
            interpolation += Time.fixedDeltaTime * speed;
        }

        current_rotation = Mathf.Lerp(current_rotation, target_rotation, interpolation);

        root.transform.localRotation = Quaternion.Euler(Vector3.up * current_rotation);
    }
}
