using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainStarter : MonoBehaviour
{
    public Train[] trains;
    private int current_train = 0;

    private GameManager gameManager;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    private void OnMouseDown()
    {
        if (gameManager.failed || gameManager.levelCompleted)
        {
            return;
        }

        if (gameManager.train_running)
        {
            return;
        }

        if (current_train >= trains.Length)
        {
            return;
        }

        gameManager.TutorialNext();

        gameManager.train_running = true;

        trains[current_train].enabled = true;
        current_train++;
    }
}
