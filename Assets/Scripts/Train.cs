using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Train : MonoBehaviour
{
    public trainColor color;
    public MeshRenderer body;

    public float max_speed;
    public float acceleration;
    private float current_speed = 0;

    private GameManager gameManager;

    private int current_point = 0;
    private float interpolation = 0;

    public Track init_track;
    public Vector3 init_point;
    public Track current_track;
    private Junction junction;
    private List<Vector3> points = new List<Vector3>();

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();

        gameManager.total_trains++;

        body.material = gameManager.Recolor(color);

        init_point = transform.position;
        current_track = init_track;

        initialize(init_track);

        enabled = false;
    }

    void initialize(Track new_track)
    {
        current_point = 0;
        interpolation = 0;
        points.Clear();

        current_track = new_track;
        junction = current_track.junction;

        points.Add(init_point);

        for (int i = 0; i < current_track.transform.childCount; i++)
        {
            points.Add(current_track.transform.GetChild(i).transform.position);
        }

        transform.position = points[current_point];
        transform.LookAt(points[current_point + 1], Vector3.up);
    }

    void change_path(Track new_track)
    {
        current_point++;

        interpolation = 0;

        current_track = new_track;
        junction = current_track.junction;

        for (int i = 0; i < current_track.transform.childCount; i++)
        {
            points.Add(current_track.transform.GetChild(i).transform.position);
        }

        transform.LookAt(points[current_point + 1], Vector3.up);
    }

    void FixedUpdate()
    {
        current_speed = Mathf.Lerp(current_speed, max_speed, Time.fixedDeltaTime * acceleration);

        if (interpolation < 1)
        {
            interpolation += Time.fixedDeltaTime * current_speed;
        }
        else
        {
            if (current_point < points.Count - 2)
            {
                current_point++;
                interpolation = 0;
                transform.LookAt(points[current_point + 1], Vector3.up);
            }
            else
            {
                if (junction == null)
                {
                    if (current_track.endColor == color)
                    {
                        Debug.Log("win");
                        gameManager.TrainReached();
                        GetComponent<Animator>().Play("TrainEnd");
                    }
                    else
                    {
                        Debug.Log("lose");
                        gameManager.Failed();
                    }
                    //initialize(init_path);
                    gameManager.train_running = false;
                    enabled = false;
                }
                else
                {
                    change_path(junction.tracks[junction.current_path]);
                }
            }
        }

        transform.position = Vector3.Lerp(points[current_point], points[current_point + 1], interpolation);
    }
}
