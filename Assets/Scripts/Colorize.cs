using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colorize : MonoBehaviour
{
    public trainColor color;

    public MeshRenderer mesh;

    GameManager gameManager;

    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();

        mesh.material = gameManager.Recolor(color);
    }
}
